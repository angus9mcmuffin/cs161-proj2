"""Secure client implementation

This is a skeleton file for you to build your secure file store client.

Fill in the methods for the class Client per the project specification.

You may add additional functions and classes as desired, as long as your
Client class conforms to the specification. Be sure to test against the
included functionality tests.
"""

from base_client import BaseClient, IntegrityError
from crypto import CryptoError
from util import *


class Client(BaseClient):
    def __init__(self, storage_server, public_key_server, crypto_object,
                 username):
        super().__init__(storage_server, public_key_server, crypto_object,
                         username)
    def create_names(self, name):
        names = []
        private_name = to_json_string(self.private_key) + name
        filename = self.crypto.cryptographic_hash(private_name, "SHA256")
        file_signame = self.crypto.cryptographic_hash(private_name + "sig", "SHA256")
        names.append(filename)
        names.append(file_signame)
        return names

    def upload(self, name, value):
        # Replace with your implementation
        # Create a symmetric key, may need more symmetric keys than one -> Need a list to carry all keys + signatures
        file_iv = self.crypto.get_random_bytes(16) # hex-string randomly generated
        file_key = self.crypto.get_random_bytes(32)
        filenames = self.create_names(name) # Ensure namespace for each user
        ciphers = []
        ciphers.append(file_iv)
        enc_file_key = self.crypto.asymmetric_encrypt(file_key, self.pks.get_public_key(self.username))
        ciphers.append(enc_file_key)
        enc_val = self.crypto.symmetric_encrypt(value, file_key, "AES", "CBC", IV=file_iv)
        ciphers.append(enc_val)
        j_ciphers = to_json_string(ciphers)
        self.storage_server.put(filenames[0], j_ciphers)
        self.storage_server.put(filenames[1], self.crypto.asymmetric_sign(j_ciphers, self.private_key))
        if not self.storage_server.get(filenames[0]) or not self.storage_server.get(filenames[1]):
            return False
        return True


    def download(self, name):
        # Replace with your implementation
        filenames = self.create_names(name)
        secfile = self.storage_server.get(filenames[0])
        sigfile = self.storage_server.get(filenames[1])
        if not sigfile or not secfile:
            return None
        if not self.crypto.asymmetric_verify(secfile, sigfile, self.pks.get_public_key(self.username)):
            raise IntegrityError
        ciphers = from_json_string(secfile)
        file_key = self.crypto.asymmetric_decrypt(ciphers[1], self.private_key)
        return self.crypto.symmetric_decrypt(ciphers[2], file_key, "AES", "CBC", IV=ciphers[0])

    def share(self, user, name):
        # Replace with your implementation (not needed for Part 1)
        raise NotImplementedError

    def receive_share(self, from_username, newname, message):
        # Replace with your implementation (not needed for Part 1)
        raise NotImplementedError

    def revoke(self, user, name):
        # Replace with your implementation (not needed for Part 1)
        raise NotImplementedError
